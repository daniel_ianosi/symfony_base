<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CartItem;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $query = $queryBuilder->select('p')
                ->from(Product::class, 'p')
                ->andWhere('p.showOnHomepage=?1')
                ->setMaxResults(4)
                ->setParameter(1, true)
                ->getQuery();

        $cartService = $this->get('cart.service');
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'products' => $query->getResult()
        ]);
    }

    /**
     * @Route("/product/{id}/{name}", name="product")
     */
    public function productAction(Request $request, $id, $name)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        return $this->render('default/product.html.twig',[
            'product' => $product
        ]);
    }


    /**
     * @Route("/category/{id}/{name}", name="category")
     */
    public function categoryAction(Request $request, $id, $name)
    {

        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        $allCategories = $category->getAllChildren();

        $em = $this->getDoctrine()->getManager();

        $query = $em->createQuery('SELECT p FROM '.Product::class.' p WHERE p.category IN (:categories)')
            ->setParameters(array('categories' => $allCategories));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            3 /*limit per page*/
        );


        return $this->render('default/category.html.twig',[
            'category' => $category,
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/search", name="search")
     */
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $q = $request->query->get('q');

        $query = $em->createQuery('SELECT p FROM '.Product::class.' p WHERE p.name like :query OR p.description like :query')
            ->setParameters(array('query' => '%'.$q.'%'));

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            3 /*limit per page*/
        );


        return $this->render('default/search.html.twig',[
            'q' => $q,
            'pagination' => $pagination
        ]);
    }

    /**
     * @Route("/cart", name="cart")
     */
    public function cartAction(Request $request)
    {
        return $this->render('default/cart.html.twig',[
            'cart' => $this->get('cart.service')->getCart()
        ]);
    }

    /**
     * @Route("/add-to-cart/{id}", name="add-to-cart")
     */
    public function addToCartAction(Request $request, $id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        $this->get('cart.service')->add($this->get('cart.service')->getCart(), $product, $request->query->get('q', 1));

        return $this->redirectToRoute('cart');
    }


    /**
     * @Route("/update-cart/{id}", name="update-cart")
     */
    public function updateCartAction(Request $request, $id)
    {
        $cartItem = $this->getDoctrine()->getRepository(CartItem::class)->find($id);
        $this->get('cart.service')->update($cartItem, $request->query->get('q'));

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/clear-cart", name="clear-cart")
     */
    public function clearCartAction(Request $request)
    {
        $this->get('cart.service')->clearCart($this->get('cart.service')->getCart());

        return $this->redirectToRoute('homepage');
    }


    /**
     * @Route("/checkout-cart", name="checkout-cart")
     */
    public function checkoutCartAction(Request $request)
    {
        $this->get('cart.service')->checkoutCart($this->get('cart.service')->getCart());

        return $this->redirectToRoute('homepage');
    }

}
